// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package protomonster

import (
	context "context"
	common "gitlab.com/vajar_minigame/proto_go/gen/common"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// MonsterServicesClient is the client API for MonsterServices service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type MonsterServicesClient interface {
	AddMon(ctx context.Context, in *Monster, opts ...grpc.CallOption) (*common.MonId, error)
	AddTestMon(ctx context.Context, in *common.UserId, opts ...grpc.CallOption) (*Monster, error)
	LockMonster(ctx context.Context, in *LockRequest, opts ...grpc.CallOption) (*Monster, error)
	ReleaseMonster(ctx context.Context, in *ReleaseRequest, opts ...grpc.CallOption) (*common.ResponseStatus, error)
	GetMonByID(ctx context.Context, in *common.MonId, opts ...grpc.CallOption) (*Monster, error)
	GetMonsByUserID(ctx context.Context, in *common.UserId, opts ...grpc.CallOption) (*MonsterList, error)
	UpdateMon(ctx context.Context, in *Monster, opts ...grpc.CallOption) (*Monster, error)
	DeleteMon(ctx context.Context, in *common.MonId, opts ...grpc.CallOption) (*common.ResponseStatus, error)
}

type monsterServicesClient struct {
	cc grpc.ClientConnInterface
}

func NewMonsterServicesClient(cc grpc.ClientConnInterface) MonsterServicesClient {
	return &monsterServicesClient{cc}
}

func (c *monsterServicesClient) AddMon(ctx context.Context, in *Monster, opts ...grpc.CallOption) (*common.MonId, error) {
	out := new(common.MonId)
	err := c.cc.Invoke(ctx, "/protomonster.MonsterServices/AddMon", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *monsterServicesClient) AddTestMon(ctx context.Context, in *common.UserId, opts ...grpc.CallOption) (*Monster, error) {
	out := new(Monster)
	err := c.cc.Invoke(ctx, "/protomonster.MonsterServices/AddTestMon", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *monsterServicesClient) LockMonster(ctx context.Context, in *LockRequest, opts ...grpc.CallOption) (*Monster, error) {
	out := new(Monster)
	err := c.cc.Invoke(ctx, "/protomonster.MonsterServices/LockMonster", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *monsterServicesClient) ReleaseMonster(ctx context.Context, in *ReleaseRequest, opts ...grpc.CallOption) (*common.ResponseStatus, error) {
	out := new(common.ResponseStatus)
	err := c.cc.Invoke(ctx, "/protomonster.MonsterServices/ReleaseMonster", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *monsterServicesClient) GetMonByID(ctx context.Context, in *common.MonId, opts ...grpc.CallOption) (*Monster, error) {
	out := new(Monster)
	err := c.cc.Invoke(ctx, "/protomonster.MonsterServices/GetMonByID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *monsterServicesClient) GetMonsByUserID(ctx context.Context, in *common.UserId, opts ...grpc.CallOption) (*MonsterList, error) {
	out := new(MonsterList)
	err := c.cc.Invoke(ctx, "/protomonster.MonsterServices/GetMonsByUserID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *monsterServicesClient) UpdateMon(ctx context.Context, in *Monster, opts ...grpc.CallOption) (*Monster, error) {
	out := new(Monster)
	err := c.cc.Invoke(ctx, "/protomonster.MonsterServices/UpdateMon", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *monsterServicesClient) DeleteMon(ctx context.Context, in *common.MonId, opts ...grpc.CallOption) (*common.ResponseStatus, error) {
	out := new(common.ResponseStatus)
	err := c.cc.Invoke(ctx, "/protomonster.MonsterServices/DeleteMon", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// MonsterServicesServer is the server API for MonsterServices service.
// All implementations must embed UnimplementedMonsterServicesServer
// for forward compatibility
type MonsterServicesServer interface {
	AddMon(context.Context, *Monster) (*common.MonId, error)
	AddTestMon(context.Context, *common.UserId) (*Monster, error)
	LockMonster(context.Context, *LockRequest) (*Monster, error)
	ReleaseMonster(context.Context, *ReleaseRequest) (*common.ResponseStatus, error)
	GetMonByID(context.Context, *common.MonId) (*Monster, error)
	GetMonsByUserID(context.Context, *common.UserId) (*MonsterList, error)
	UpdateMon(context.Context, *Monster) (*Monster, error)
	DeleteMon(context.Context, *common.MonId) (*common.ResponseStatus, error)
	mustEmbedUnimplementedMonsterServicesServer()
}

// UnimplementedMonsterServicesServer must be embedded to have forward compatible implementations.
type UnimplementedMonsterServicesServer struct {
}

func (UnimplementedMonsterServicesServer) AddMon(context.Context, *Monster) (*common.MonId, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddMon not implemented")
}
func (UnimplementedMonsterServicesServer) AddTestMon(context.Context, *common.UserId) (*Monster, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddTestMon not implemented")
}
func (UnimplementedMonsterServicesServer) LockMonster(context.Context, *LockRequest) (*Monster, error) {
	return nil, status.Errorf(codes.Unimplemented, "method LockMonster not implemented")
}
func (UnimplementedMonsterServicesServer) ReleaseMonster(context.Context, *ReleaseRequest) (*common.ResponseStatus, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReleaseMonster not implemented")
}
func (UnimplementedMonsterServicesServer) GetMonByID(context.Context, *common.MonId) (*Monster, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMonByID not implemented")
}
func (UnimplementedMonsterServicesServer) GetMonsByUserID(context.Context, *common.UserId) (*MonsterList, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMonsByUserID not implemented")
}
func (UnimplementedMonsterServicesServer) UpdateMon(context.Context, *Monster) (*Monster, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateMon not implemented")
}
func (UnimplementedMonsterServicesServer) DeleteMon(context.Context, *common.MonId) (*common.ResponseStatus, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteMon not implemented")
}
func (UnimplementedMonsterServicesServer) mustEmbedUnimplementedMonsterServicesServer() {}

// UnsafeMonsterServicesServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to MonsterServicesServer will
// result in compilation errors.
type UnsafeMonsterServicesServer interface {
	mustEmbedUnimplementedMonsterServicesServer()
}

func RegisterMonsterServicesServer(s grpc.ServiceRegistrar, srv MonsterServicesServer) {
	s.RegisterService(&MonsterServices_ServiceDesc, srv)
}

func _MonsterServices_AddMon_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Monster)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MonsterServicesServer).AddMon(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protomonster.MonsterServices/AddMon",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MonsterServicesServer).AddMon(ctx, req.(*Monster))
	}
	return interceptor(ctx, in, info, handler)
}

func _MonsterServices_AddTestMon_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(common.UserId)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MonsterServicesServer).AddTestMon(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protomonster.MonsterServices/AddTestMon",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MonsterServicesServer).AddTestMon(ctx, req.(*common.UserId))
	}
	return interceptor(ctx, in, info, handler)
}

func _MonsterServices_LockMonster_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(LockRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MonsterServicesServer).LockMonster(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protomonster.MonsterServices/LockMonster",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MonsterServicesServer).LockMonster(ctx, req.(*LockRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MonsterServices_ReleaseMonster_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReleaseRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MonsterServicesServer).ReleaseMonster(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protomonster.MonsterServices/ReleaseMonster",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MonsterServicesServer).ReleaseMonster(ctx, req.(*ReleaseRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MonsterServices_GetMonByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(common.MonId)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MonsterServicesServer).GetMonByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protomonster.MonsterServices/GetMonByID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MonsterServicesServer).GetMonByID(ctx, req.(*common.MonId))
	}
	return interceptor(ctx, in, info, handler)
}

func _MonsterServices_GetMonsByUserID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(common.UserId)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MonsterServicesServer).GetMonsByUserID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protomonster.MonsterServices/GetMonsByUserID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MonsterServicesServer).GetMonsByUserID(ctx, req.(*common.UserId))
	}
	return interceptor(ctx, in, info, handler)
}

func _MonsterServices_UpdateMon_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Monster)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MonsterServicesServer).UpdateMon(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protomonster.MonsterServices/UpdateMon",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MonsterServicesServer).UpdateMon(ctx, req.(*Monster))
	}
	return interceptor(ctx, in, info, handler)
}

func _MonsterServices_DeleteMon_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(common.MonId)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MonsterServicesServer).DeleteMon(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protomonster.MonsterServices/DeleteMon",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MonsterServicesServer).DeleteMon(ctx, req.(*common.MonId))
	}
	return interceptor(ctx, in, info, handler)
}

// MonsterServices_ServiceDesc is the grpc.ServiceDesc for MonsterServices service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var MonsterServices_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "protomonster.MonsterServices",
	HandlerType: (*MonsterServicesServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "AddMon",
			Handler:    _MonsterServices_AddMon_Handler,
		},
		{
			MethodName: "AddTestMon",
			Handler:    _MonsterServices_AddTestMon_Handler,
		},
		{
			MethodName: "LockMonster",
			Handler:    _MonsterServices_LockMonster_Handler,
		},
		{
			MethodName: "ReleaseMonster",
			Handler:    _MonsterServices_ReleaseMonster_Handler,
		},
		{
			MethodName: "GetMonByID",
			Handler:    _MonsterServices_GetMonByID_Handler,
		},
		{
			MethodName: "GetMonsByUserID",
			Handler:    _MonsterServices_GetMonsByUserID_Handler,
		},
		{
			MethodName: "UpdateMon",
			Handler:    _MonsterServices_UpdateMon_Handler,
		},
		{
			MethodName: "DeleteMon",
			Handler:    _MonsterServices_DeleteMon_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "monster.proto",
}
