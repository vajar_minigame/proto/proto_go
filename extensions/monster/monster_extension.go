package monster

import "gitlab.com/vajar_minigame/proto_go/gen/monster"

//AddHP adds the wanted hp, can be negative as well
func AddHP(b *monster.BattleValues, hp int32) {

	b.RemainingHp += hp
	if b.RemainingHp < 0 {
		b.RemainingHp = 0
	}
	if b.RemainingHp > b.MaxHp {
		b.RemainingHp = b.MaxHp
	}

}

//AddSaturation adds the wanted saturation, can be negative as well
func AddSaturation(b *monster.BodyValues, sat int) {

	b.RemainingSaturation += float64(sat)
	if b.RemainingSaturation < 0 {
		b.RemainingSaturation = 0
	}
	if b.RemainingSaturation > b.MaxSaturation {
		b.RemainingSaturation = b.MaxSaturation
	}
}
