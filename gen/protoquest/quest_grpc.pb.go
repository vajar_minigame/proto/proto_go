// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package protoquest

import (
	context "context"
	common "gitlab.com/vajar_minigame/proto_go/gen/common"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// QuestServicesClient is the client API for QuestServices service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type QuestServicesClient interface {
	AddQuest(ctx context.Context, in *Quest, opts ...grpc.CallOption) (*common.QuestId, error)
	GetQuestByID(ctx context.Context, in *common.QuestId, opts ...grpc.CallOption) (*Quest, error)
	GetQuestByUserID(ctx context.Context, in *common.UserId, opts ...grpc.CallOption) (*QuestList, error)
	StartQuest(ctx context.Context, in *StartQuestRequest, opts ...grpc.CallOption) (*common.ResponseStatus, error)
}

type questServicesClient struct {
	cc grpc.ClientConnInterface
}

func NewQuestServicesClient(cc grpc.ClientConnInterface) QuestServicesClient {
	return &questServicesClient{cc}
}

func (c *questServicesClient) AddQuest(ctx context.Context, in *Quest, opts ...grpc.CallOption) (*common.QuestId, error) {
	out := new(common.QuestId)
	err := c.cc.Invoke(ctx, "/protoquest.QuestServices/AddQuest", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *questServicesClient) GetQuestByID(ctx context.Context, in *common.QuestId, opts ...grpc.CallOption) (*Quest, error) {
	out := new(Quest)
	err := c.cc.Invoke(ctx, "/protoquest.QuestServices/GetQuestByID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *questServicesClient) GetQuestByUserID(ctx context.Context, in *common.UserId, opts ...grpc.CallOption) (*QuestList, error) {
	out := new(QuestList)
	err := c.cc.Invoke(ctx, "/protoquest.QuestServices/GetQuestByUserID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *questServicesClient) StartQuest(ctx context.Context, in *StartQuestRequest, opts ...grpc.CallOption) (*common.ResponseStatus, error) {
	out := new(common.ResponseStatus)
	err := c.cc.Invoke(ctx, "/protoquest.QuestServices/StartQuest", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// QuestServicesServer is the server API for QuestServices service.
// All implementations must embed UnimplementedQuestServicesServer
// for forward compatibility
type QuestServicesServer interface {
	AddQuest(context.Context, *Quest) (*common.QuestId, error)
	GetQuestByID(context.Context, *common.QuestId) (*Quest, error)
	GetQuestByUserID(context.Context, *common.UserId) (*QuestList, error)
	StartQuest(context.Context, *StartQuestRequest) (*common.ResponseStatus, error)
	mustEmbedUnimplementedQuestServicesServer()
}

// UnimplementedQuestServicesServer must be embedded to have forward compatible implementations.
type UnimplementedQuestServicesServer struct {
}

func (UnimplementedQuestServicesServer) AddQuest(context.Context, *Quest) (*common.QuestId, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddQuest not implemented")
}
func (UnimplementedQuestServicesServer) GetQuestByID(context.Context, *common.QuestId) (*Quest, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetQuestByID not implemented")
}
func (UnimplementedQuestServicesServer) GetQuestByUserID(context.Context, *common.UserId) (*QuestList, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetQuestByUserID not implemented")
}
func (UnimplementedQuestServicesServer) StartQuest(context.Context, *StartQuestRequest) (*common.ResponseStatus, error) {
	return nil, status.Errorf(codes.Unimplemented, "method StartQuest not implemented")
}
func (UnimplementedQuestServicesServer) mustEmbedUnimplementedQuestServicesServer() {}

// UnsafeQuestServicesServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to QuestServicesServer will
// result in compilation errors.
type UnsafeQuestServicesServer interface {
	mustEmbedUnimplementedQuestServicesServer()
}

func RegisterQuestServicesServer(s grpc.ServiceRegistrar, srv QuestServicesServer) {
	s.RegisterService(&QuestServices_ServiceDesc, srv)
}

func _QuestServices_AddQuest_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Quest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(QuestServicesServer).AddQuest(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protoquest.QuestServices/AddQuest",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(QuestServicesServer).AddQuest(ctx, req.(*Quest))
	}
	return interceptor(ctx, in, info, handler)
}

func _QuestServices_GetQuestByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(common.QuestId)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(QuestServicesServer).GetQuestByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protoquest.QuestServices/GetQuestByID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(QuestServicesServer).GetQuestByID(ctx, req.(*common.QuestId))
	}
	return interceptor(ctx, in, info, handler)
}

func _QuestServices_GetQuestByUserID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(common.UserId)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(QuestServicesServer).GetQuestByUserID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protoquest.QuestServices/GetQuestByUserID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(QuestServicesServer).GetQuestByUserID(ctx, req.(*common.UserId))
	}
	return interceptor(ctx, in, info, handler)
}

func _QuestServices_StartQuest_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(StartQuestRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(QuestServicesServer).StartQuest(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protoquest.QuestServices/StartQuest",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(QuestServicesServer).StartQuest(ctx, req.(*StartQuestRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// QuestServices_ServiceDesc is the grpc.ServiceDesc for QuestServices service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var QuestServices_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "protoquest.QuestServices",
	HandlerType: (*QuestServicesServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "AddQuest",
			Handler:    _QuestServices_AddQuest_Handler,
		},
		{
			MethodName: "GetQuestByID",
			Handler:    _QuestServices_GetQuestByID_Handler,
		},
		{
			MethodName: "GetQuestByUserID",
			Handler:    _QuestServices_GetQuestByUserID_Handler,
		},
		{
			MethodName: "StartQuest",
			Handler:    _QuestServices_StartQuest_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "quest.proto",
}
