#!/bin/bash

git rm -rf gen/*
export GO111MODULE=on
#todo change to new as soon as grpc is available google.golang.org/protobuf/cmd/protoc-gen-go
go get -u google.golang.org/protobuf/cmd/protoc-gen-go
go get -u google.golang.org/grpc/cmd/protoc-gen-go-grpc
go get -u google.golang.org/grpc


protoFiles=`find minigame_proto/proto/*.proto`
for file in $protoFiles
do
echo $file
protoc -I=minigame_proto/proto/ --go_out=module=gitlab.com/vajar_minigame/proto_go:. ${file}
protoc -I=minigame_proto/proto/ --go-grpc_out=module=gitlab.com/vajar_minigame/proto_go:. ${file}
done
